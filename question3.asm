# Program to display the values in 5 cells

	.text

main:	

		li $v0, 4				#print string mode
		la $a0, hello_msg
		syscall

############## Reading ############################
		
Read1:
		li $v0, 8 #take in input
		la $a0, buffer0
		li $a1, 20
		move $t0, $a0
		syscall
 
Read2:	
		li $v0, 8 #take in input
		la $a0, buffer1
		li $a1, 20
		move $t1, $a0
		syscall

Check2: 
		lb $t5, 0($t1) 				# load first char
		bne $t5, '=', Read3			#check for equals sign

		lb $t5, 1($t1)				# load second char
		bne $t5, '0', Read3			#check =1 entered

		move $t1, $t0				# cell now =1

Read3: 		
		li $v0, 8 #take in input
		la $a0, buffer2
		li $a1, 20
		move $t2, $a0
		syscall

Check3: 
		lb $t6, 0($t2) 				# load first char
		bne $t6, '=', Read4			#check for equals sign

		lb $t6, 1($t2)				# load second char
		bne $t6, '0', Check3if2		#check =1 entered
		move $t2, $t0				# cell now =1
		j Read4

Check3if2:
		lb $t6, 1($t2)				# load second char
		bne $t6, '1', Read4	
		move $t2, $t1

						# cell now =1


Read4:
		xor $a0, $a0, $a0
 		li $v0, 8 #take in input
		la $a0, buffer3
		li $a1, 20
		move $t3, $a0
		syscall
 		
Check4: 
		lb $t6, 0($t3) 				# load first char
		bne $t6, '=', Read5			#check for equals sign

		lb $t6, 1($t3)				# load second char
		bne $t6, '0', Check4if2		#check =1 entered
		move $t3, $t0				# cell now =1
		j Read5

Check4if2:
		
		lb $t6, 1($t3)				# load second char
		bne $t6, '1', Check4if3	
		move $t3, $t1
		j Read5

Check4if3:
		
		lb $t6, 1($t3)				# load second char
		bne $t6, '2', Read5	
		move $t3, $t2

Read5:
 		li $v0, 8 #take in input
		la $a0, buffer4
		li $a1, 20
		move $t4, $a0
		syscall

Check5: 
		lb $t6, 0($t4) 				# load first char
		bne $t6, '=', Done			#check for equals sign

		lb $t6, 1($t4)				# load second char
		bne $t6, '0', Check5if2		#check =1 entered
		move $t4, $t0				# cell now =1
		j Done

Check5if2:
		
		lb $t6, 1($t4)				# load second char
		bne $t6, '1', Check5if3	
		move $t4, $t1
		j Done

Check5if3:
		
		lb $t6, 1($t4)				# load second char
		bne $t6, '2', Check5if4
		move $t4, $t2
		j Done
 
Check5if4:
		
		lb $t6, 1($t4)				# load second char
		bne $t6, '3', Done	
		move $t4, $t3


############# Done reading ##########################

Done:
		li $v0, 4				#print string mode
		la $a0, ans_msg
		syscall

############# Check for equal ############################


############# Writing ####################################
		
Write:
		
		xor $a0, $a0, $a0
		move $a0, $t0
		li $v0, 4		#print string mode
		syscall


		move $a0, $t1
		syscall


		move $a0, $t2
		syscall
		

		move $a0, $t3
		syscall
		
		move $a0, $t4
		syscall

		li $v0, 10
		syscall



################## data for the program ##############################
	.data

hello_msg:	
		
		.asciiz "Enter a series of 5 formulae:\n"


ans_msg:
	
		.asciiz "The values are:\n"

true_msg:
		
		.asciiz "True\n"
buffer0: 
		.space 30

buffer1: 
		.space 30

buffer2: 
		.space 30

buffer3: 
		.space 30

buffer4: 
		.space 30				


new_line:
		.asciiz "\n"

# End