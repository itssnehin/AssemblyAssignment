
.text
 main:
    #Printing the message
    li $v0, 4
    la $a0, read_string
    syscall

    #Saving the text
    li $v0, 8
    la $a0, input_str
    li $a1, 20
    syscall
    #Count the length of the string 
    la $t0, input_str   
    li $t3, 0   
 countChr:  
        lb $t2, 0($t0)  # Load the first byte from address in $t0  
        beqz $t2, pot   # if $t2 == 0 then go to pot  
        add $t0, $t0, 1  
        add $t3, $t3, 1 # increment counter  
        j countChr      # finally loop  
 pot:
    li $t1, 1    # $t1 is the counter. set it to 1  
    li $t2, 0   #$t2 is the sum
    sub $t3, $t3, $t1


convertint:  
    lbu $t0, input_str($t1)  # Load the first byte from address in $t0    
    beq $t1, $t3, calculate
    addi $t0, $t0, -48   #converts t1's ascii value to dec value
    mul $t2, $t2, 10    #sum *= 10
    add $t2, $t2, $t0 
    addi $t1, $t1, 1     #increment array address
    j convertint      # loop  


calculate:
    #Add 5 to number
    addi $t2, $t2, 5

    #print out second message and text
    li $v0, 4
    la $a0, outmsg
    syscall

    #Gets the right answer
    li $v0, 1 	#print int
    add $a0, $zero, $t2
    syscall

    #exit
    li $v0, 10
    syscall

.data

input_str: 
		.space 30
counter:
		 .space 30

read_string: 			
		.asciiz "Enter a string:\n"

outmsg: 
		.asciiz "The value +5 is:\n"
