# Program to display 5 strings that are inputted

	.text

main:	

		li $v0, 4				#print string mode
		la $a0, hello_msg
		syscall

############## Reading ############################
		
Read:
		li $v0, 8 #take in input
		la $a0, buffer0
		li $a1, 20
		move $t0, $a0
		syscall
 
		
		li $v0, 8 #take in input
		la $a0, buffer1
		li $a1, 20
		move $t1, $a0
		syscall


 		li $v0, 8 #take in input
		la $a0, buffer2
		li $a1, 20
		move $t2, $a0
		syscall
 		
		xor $a0, $a0, $a0
 		li $v0, 8 #take in input
		la $a0, buffer3
		li $a1, 20
		move $t3, $a0
		syscall
 		

 		li $v0, 8 #take in input
		la $a0, buffer4
		li $a1, 20
		move $t4, $a0
		syscall
 
############# Done reading ##########################

		li $v0, 4				#print string mode
		la $a0, ans_msg
		syscall

############# Writing ####################################
		
Write:
		
		xor $a0, $a0, $a0
		move $a0, $t0
		li $v0, 4		#print string mode
		syscall


		move $a0, $t1
		syscall


		move $a0, $t2
		syscall
		

		move $a0, $t3
		syscall
		
		move $a0, $t4
		syscall

		li $v0, 10
		syscall



################## data for the program ##############################
	.data

hello_msg:	
		
		.asciiz "Enter a series of 5 formulae:\n"


ans_msg:
	
		.asciiz "The values are:\n"

buffer0: 
		.space 30

buffer1: 
		.space 30

buffer2: 
		.space 30

buffer3: 
		.space 30

buffer4: 
		.space 30				


new_line:
		.asciiz "\n"

# End